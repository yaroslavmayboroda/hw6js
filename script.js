'use script'

function createNewUser() {
    var newUser = {};
    newUser.firstName = prompt("Введите своё имя:");
    newUser.lastName = prompt("Введите свою фамилию:");

    var birthdayInput = prompt("Введите свою дату рождения (в формате dd.mm.yyyy):");
    var birthdayParts = birthdayInput.split(".");
    var birthday = new Date(
        parseInt(birthdayParts[2]),
        parseInt(birthdayParts[1]) - 1,
        parseInt(birthdayParts[0])
    );
    newUser.birthday = birthday;

    newUser.getAge = function () {
        var currentDate = new Date();
        var age = currentDate.getFullYear() - this.birthday.getFullYear();
        var currentMonth = currentDate.getMonth();
        var birthdayMonth = this.birthday.getMonth();

        if (
            birthdayMonth > currentMonth ||
            (birthdayMonth === currentMonth && this.birthday.getDate() > currentDate.getDate())
        ) {
            age--;
        }
        return age;
    };

    newUser.getPassword = function () {
        var password =
        this.firstName.charAt(0).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.getFullYear();

        return password;
    };

    return newUser;
}

var user = createNewUser();

console.log("Имя: " + user.firstName);
console.log("Фамилия: " + user.lastName);
console.log("Дата рождения: " + user.birthday.toLocaleDateString());
console.log("Возраст: " + user.getAge());